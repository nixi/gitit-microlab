<div class="sitenav">
  <fieldset>
    <legend>Wiki</legend>
    <ul>
      <li><a href="$base$/">Úvodní stránka</a></li>
      <li><a href="$base$/_index">Všechny stránky</a></li>
      <li><a href="$base$/_categories">Kategorie</a></li>
      <li><a href="$base$/_random">Náhodná stránka</a></li>
      <li><a href="$base$/_activity">Poslední aktivita</a></li>
      $if(wikiupload)$
        <li><a href="$base$/_upload">Nahrát soubor</a></li>
      $endif$
      $if(feed)$
      <li><a href="$base$/_feed/" type="application/atom+xml" rel="alternate" title="ATOM Feed">Atom feed</a> <img alt="feed icon" src="$base$/img/icons/feed.png"/></li>
      $endif$
      <li><a href="$base$/Help">Pomoc</a></li>
    </ul>
    <form action="$base$/_search" method="get" id="searchform">
     <input type="text" name="patterns" id="patterns"/>
     <input type="submit" name="search" id="search" value="Hledat"/>
    </form>
    <form action="$base$/_go" method="post" id="goform">
      <input type="text" name="gotopage" id="gotopage"/>
      <input type="submit" name="go" id="go" value="Přejít"/>
    </form>
  </fieldset>
</div>

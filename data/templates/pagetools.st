<div class="pageTools">
  <fieldset>
    <legend>Tato stránka</legend>
    <ul>
      <li><a href="$base$/_showraw$pageUrl$$if(revision)$?revision=$revision$$endif$">Zdroj stránky</a></li>
      <li><a href="$base$$pageUrl$?printable$if(revision)$&amp;revision=$revision$$endif$">Verze pro tisk</a></li>
      <li><a href="$base$/_delete$pageUrl$">Smazat stránku</a></li>
      $if(feed)$
      <li><a href="$base$/_feed$pageUrl$" type="application/atom+xml" rel="alternate" title="ATOM Feed této stránky">Atom feed</a> <img alt="feed icon" src="$base$/img/icons/feed.png"/></li>
      $endif$
    </ul>
    $exportbox$
  </fieldset>
</div>

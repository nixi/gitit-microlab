<div id="content">
  $if(revision)$
    <h2 class="revision">Revize $revision$ (Po kliknutí na název stránky se zobrazí aktuální revize)</h2>
  $endif$
  <h1 class="pageTitle"><a href="$base$$pageUrl$">$pagetitle$</a></h1>
  $if(messages)$
    $messages()$
  $endif$
  $content$
</div>
